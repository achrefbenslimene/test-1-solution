const Logo = () => {
  return (
    <div className='logo'>
      Taki<span className='text-secondary'>Academy</span>
    </div>
  );
};

export default Logo;
