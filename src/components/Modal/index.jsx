import useModal from "../../hooks/useModal";

const Modal = () => {
  const { modal, closeModal } = useModal();

  return (
    <div className={modal.isOpen === true ? "modal showed" : "modal"}>
      <div className='container'>
        <h2>{modal?.data?.subject?.name}</h2>
        <p>{modal?.data?.subject?.description}</p>
        <div className='buttons-group'>
          <button
            className='secondary'
            onClick={() => {
              modal?.data?.deleteSubject((prev) => {
                return prev.filter(
                  (subject) => subject.id !== modal?.data?.subject?.id
                );
              });
              closeModal();
            }}
          >
            Supprimer
          </button>
          <button onClick={() => closeModal()}>Fermer</button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
