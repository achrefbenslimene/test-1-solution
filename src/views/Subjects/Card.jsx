import useModal from "../../hooks/useModal";
import shortener from "../../utils/textShortener";

const Card = ({ subject, deleteSubject }) => {
  const { showModal } = useModal();
  const { name, description } = subject;
  return (
    <div className='card' onClick={() => showModal({ subject, deleteSubject })}>
      <h2>{shortener(name, 10, "..")}</h2>
      <p>{shortener(description, 64)}</p>
    </div>
  );
};

export default Card;
