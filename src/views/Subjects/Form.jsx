import { useState } from "react";

const initialValidation = {
  name: {
    value: "",
    error: false,
  },
  description: {
    value: "",
    error: false,
  },
};

const Form = ({ addSubject }) => {
  const [validation, setValidation] = useState(initialValidation);

  const hundleSubmit = () => {
    if (validation.name.value === "") {
      setValidation((prev) => {
        return {
          ...prev,
          name: {
            ...prev.name,
            error: true,
          },
        };
      });
      return;
    }
    if (validation.description.value === "") {
      setValidation((prev) => {
        return {
          ...prev,
          description: {
            ...prev.description,
            error: true,
          },
        };
      });
      return;
    }
    addSubject((prev) => {
      return [
        ...prev,
        {
          id: prev.length + 1,
          name: validation.name.value,
          description: validation.description.value,
        },
      ];
    });
    setValidation(initialValidation);
  };

  return (
    <div className='form'>
      <h2>Ajouter une matière</h2>
      <input
        className={validation.name.error === true ? "error" : ""}
        type='text'
        placeholder='Nom'
        value={validation.name.value}
        onChange={(e) => {
          setValidation((prev) => {
            return {
              ...prev,
              name: {
                ...prev.name,
                value: e.target.value,
                error: false,
              },
            };
          });
        }}
      />
      <textarea
        className={validation.description.error === true ? "error" : ""}
        placeholder='Description'
        rows='5'
        value={validation.description.value}
        onChange={(e) => {
          setValidation((prev) => {
            return {
              ...prev,
              description: {
                ...prev.description,
                value: e.target.value,
                error: false,
              },
            };
          });
        }}
      ></textarea>
      <button onClick={() => hundleSubmit()}>Ajouter</button>
    </div>
  );
};

export default Form;
